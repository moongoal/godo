/*
 * Copyright (c) 2016, Alfredo Mungo
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef _GLOBALS_H
#define _GLOBALS_H
/*
  Declare and define global variables.

  Variable definition is performed in every file defining `DEFINE_GLOBALS`,
  meaning that only one such file shall exist. Files not containing the
  definition will declare the same variables as extern.
*/

#include <stdbool.h>

/* Program execution mode */
#define MODE_RUN 1
#define MODE_PARSEONLY 2

/*
  Decide whether to define globals or just declare them for usage.
*/
#ifdef DEFINE_GLOBALS
  #define EXTDECL(var, value) var = value
  #define extern
#else /* DEFINE_GLOBALS */
  #define EXTDECL(var, value) extern var
#endif /* DEFINE_GLOBALS */

extern char shellbuff[SHELLBUFF_LEN]; /* Shell path buffer */
extern char cl_username[IDENTITY_LEN]; /* Command line given username */
extern char cl_group[IDENTITY_LEN]; /* Command line given group */
extern char cfgpath[CFGPATH_LEN]; /* Configuration file path buffer */

EXTDECL(char **cl_command, NULL); /* Command & args given on command line */
EXTDECL(size_t cl_command_len, 0); /* Length of cl_command */

EXTDECL(char *shellbuff_ptr, shellbuff); /* Pointer version of shellbuff */

EXTDECL(int mode, MODE_RUN); /* Program execution mode */
EXTDECL(bool dry_run, false); /* Dry-run enabled flag */
EXTDECL(int exit_value, EXIT_SUCCESS); /* Program exit value */

#undef EXTDECL

#ifdef extern
  #undef extern
#endif

#endif /* _GLOBALS_H */
