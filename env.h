/*
 * Copyright (c) 2016, Alfredo Mungo
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef _ENV_H
#define _ENV_H

#include <stdbool.h>

/*
  Copies the name of an environment string (in the format of `environ` strings).
  If the output buffer is not large enough, the result is truncated.

  ARGUMENTS
    envstr: The environment string
    outname: The output buffer
    outname_len: The output buffer length in characters

  RETURN VALUE
    True if the output buffer contains the full lenght variable name or false
    if the name is truncated or an error occurred (in that case `outname` will
    have it's first character set to '\0').
*/
bool getenv_name(const char *envstr, char *outname, size_t outname_len);

/*
  Return a pointer to the environment variable content (e.g. "name=value") from
  array.

  ARGUMENTS
    env: NULL-terminated array of environment variables
    name: Environment variable name to look for

  RETURN VALUE
    Pointer the string in `env` containing the variable content or NULL if no
    such name is present in the given array.
*/
char **getenv_content_ptr_from_array(char **env, const char *name);

/*
  Returns a new list of environment variable names.

  ARGUMENTS
    env: The environment variables array

  RETURN VALUE
    A NULL-terminated pointer to a list of strings containing each a name of
    a currently defined environment variable. The pointer (and the pointers it
    points to) must be freed by the calling routine.
*/
char **getenv_all_names(char **env);

/*
  Set the given variable into the given environment array.

  ARGUMENTS
    env: The null-terminated environment array, which is assumed to have
      enough space to contain the given entry and all unused pointer slots
      set to NULL.
    name: The environment variable name
    value: The environment variable value (can be NULL)
*/
void setenv2(char **env, const char *name, const char *value);

#endif /* _ENV_H */
