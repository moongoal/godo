/*
 * Copyright (c) 2016, Alfredo Mungo
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
*/
#define DEFINE_GLOBALS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>
#ifndef NDEBUG
  #include <unistd.h>
  #include <signal.h>
#endif
#include "cmdline.h"
#include "parser-utils.h"
#include "match.h"
#include "login.h"
#include "runcmd.h"
#include "cfgfile.h"
#include "y.tab.h"
#include "config.h"
#include "debug.h"

int main(int argc, char **argv) {
  dbgnotice2("%s v%s %s\n", APP_NAME, APP_VERSION, APP_VERSION_PHASE);
  dbgnotice2("Running as EUID=%llu, UID=%llu...\n", (unsigned long long)geteuid(), (unsigned long long)getuid());

  /* kill(getpid(), SIGSTOP); */

  /* Set default umask */
  umask(0133);

  /* Initialize */
  strncpy(cfgpath, CONFIG_PATH, CFGPATH_LEN);
  strncpy(cl_username, DEFAULT_USER, IDENTITY_LEN);
  strncpy(cl_group, DEFAULT_GROUP, IDENTITY_LEN);

  dbgnotice2("Configuration file is: %s\n", cfgpath);

  /* Parse command line */
  parse_cmdline(argc, argv);

  /* Check permissions */
  if(mode != MODE_PARSEONLY) {
    if(!check_cfg_perms()) {
      struct passwd *pwd;

      if((pwd = getpwnam("root"))) {
        struct group *grp;

        if((grp = getgrgid(pwd->pw_gid)))
          dbgerr2("Configuration file has wrong permissions/owner. Please make it owned by root:%s, with no write permissions for group/others\n", grp->gr_name);
        else
          dbgerr("Can't retrieve group for user root\n");
      } else
        dbgerr("Can't retrieve entry for user root\n");

      return EXIT_FAILURE;
    }
  }
  
  /* Parse config file */
  if(!parse_cfg()) {
    dbgerr("Error reading the configuration file\n");
    return EXIT_FAILURE;
  }

  /* Perform action */
  switch(mode) {
    case MODE_PARSEONLY:
      break;
    case MODE_RUN:
      if(cl_command)
        run();
      else {
        dbgerr("You must specify a command or the -s switch\n");
        exit_value = EXIT_FAILURE;
      }
      break;
    default:
      abort();
  }

  /* Cleanup & exit */
  cleanup();

  return exit_value;
}
