/*
 * Copyright (c) 2016, Alfredo Mungo
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
*/
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include "debug.h"
#include "cmdline.h"
#include "config.h"
#include "login.h"

static void print_help() {
  printf("%s - execute commands as another user/group\n\n", APP_NAME);

  printf("Syntax:\n\t%s [OPTIONS] {COMMAND} {ARGS}\n\t%s [OPTIONS] -s\n", APP_NAME, APP_NAME);

  puts("");
  puts("Options:");
  puts("-C PATH\n\tOnly parse the given file");
  puts("-s\n\tRun your preferred shell");
  puts("-u TARGET\n\tRun command as given identity");
  puts("-h\n\tPrint this message and exit");
  
  puts("");
  puts("For further information run `man 1 godo`.");
}

void parse_cmdline(int argc, char **argv) {
  int opt;
  static const char *optstrings = "Csuh"
  #ifndef NDEBUG
  "n"
  #endif
  ;
  extern int optind;

  while((opt = getopt(argc, argv, optstrings)) != -1) {
    switch(opt) {
      case 'C':
          mode = MODE_PARSEONLY;
          
          if(optind >= argc) {
            dbgerr("-C requires an argument\n");
            exit(EXIT_FAILURE);
          } else
            strncpy(cfgpath, argv[optind++], CFGPATH_LEN);

          break;
      case 's':
        strncpy(shellbuff, get_shell(), SHELLBUFF_LEN);
        cl_command = &shellbuff_ptr;
        cl_command_len = 1;
        break;
      case 'u':
        if(optind >= argc) {
          dbgerr("-u requires an argument\n");
          exit(EXIT_FAILURE);
        } else {
          char *cl_ident = argv[optind++];
          
          if(cl_ident[0] == ':') {
            strncpy(cl_group, &cl_ident[1], IDENTITY_LEN);
          } else {
            char *ident_status = NULL;
            char *ident = strtok_r(cl_ident, ":", &ident_status);

            if(ident) {
              strncpy(cl_username, ident, IDENTITY_LEN);

              ident = strtok_r(NULL, ":", &ident_status);

              if(ident)
                strncpy(cl_group, ident, IDENTITY_LEN);
            } else {
              dbgerr("Invalid identity supplied to -u.\n");
              exit(EXIT_FAILURE);
            }
          }
        }
        break;
      #ifndef NDEBUG
      case 'n':
        dry_run = true;
        break;
      #endif
      case 'h':
        print_help();
        exit(EXIT_SUCCESS);
        break;
      case '?':
        exit(EXIT_FAILURE);
        break;
    }
  }

  if(!cl_command && optind < argc) { /* Command defined on CL */
    cl_command = &argv[optind];
    cl_command_len = argc - optind;
  }

  dbgnotice2("Command is: %s, with %lu arguments\n", cl_command? cl_command[0]: "(none)", cl_command? (unsigned long)cl_command_len - 1: 0);
}
