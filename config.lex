/*
 * Copyright (c) 2016, Alfredo Mungo
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
*/
%array
%option stack

%x setenv setenv_value_word
%x cmd args
%x string word

%{
#include <stdlib.h>
#include "y.tab.h"
#include "parser-utils.h"
#define BEGIN_STRING do { ndelim = 0; yy_push_state(string); } while(0)

static unsigned linen = 1;
static unsigned ndelim = 0;
%}

EOR                 "\n"
designator          [a-z_:][a-z0-9_:]*
varname             [a-zA-Z_][A-Za-z0-9_]*

%%
{EOR}                                                 { linen++; return EOR; }
^[ \t]*#.*$                                           { return COMMENT; }
^[ \t]*permit/[ \t]+                                  { return PERMIT; }
^[ \t]*deny/[ \t]+                                    { return DENY; }
"("                                                   { return BR_OPEN; }
")"                                                   { return BR_CLOSE; }
","                                                   { return COMMA; }
keepenv/([ \t]*|,)                                    { return KEEPENV; }
nopass/([ \t]*|,)                                     { return NOPASS; }
persist/([ \t]*|,)                                    { return PERSIST; }
setenv/[ \t]*\{                                       { BEGIN setenv; return SETENV; }
"{"                                                   { return BLOCK_OPEN; }
"}"                                                   { return BLOCK_CLOSE; }
as/[ \t]                                              { return AS; }
cmd[ \t]+                                             { ndelim = 0; BEGIN cmd; return CMD; }
args/([ \t]|{EOR})                                    { cur_rule.has_args = true; BEGIN args; return ARGS; }
{designator}                                          { return LOGIN; }

<setenv>\{                                            { return BLOCK_OPEN; }
<setenv>[ \t]*\}                                      { BEGIN INITIAL; return BLOCK_CLOSE; }
<setenv>=/\"                                          { BEGIN_STRING; return EQ; }
<setenv>=                                             { yy_push_state(setenv_value_word); return EQ; }
<setenv>{varname}                                     { return VARNAME; }

<string>\"                                            { if(ndelim++) yy_pop_state(); return STRING_DELIM; }
<string>[^"]+                                         { return STRING; }

<setenv_value_word>[^ \t}]+                           { return STRING; }
<setenv_value_word>[ \t]*\}                           { BEGIN INITIAL; return BLOCK_CLOSE; }

<word,setenv_value_word>[ \t]+                        { yy_pop_state(); }
<word>[^ \t]+                                         { return STRING; }

<args>\"                                              { unput('"'); BEGIN_STRING; }
<cmd,args>{EOR}                                       { unput('\n'); BEGIN INITIAL; }
<args>[^ \t\n]+                                       { if(yytext[0] == '"') REJECT; return STRING; }

<cmd>[ \t]+                                           { BEGIN INITIAL; }
<cmd>[^ \t\n]+                                        { return COMMAND; }

<*>[ \t]+                                             ;

%%

void yyerror(const char *s) {
  fprintf(stderr, "Configuration file parse error on line %u: %s\n", linen, s);
  exit(EXIT_FAILURE);
}

int yywrap() {
  return 1;
}
