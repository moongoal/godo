# Debug configuration
# DEBUG=1

# Application configuration
export APP_VERSION_MAJOR=0
export APP_VERSION_MINOR=0
export APP_VERSION_PATCH=1
export APP_VERSION_PHASE=beta
ifdef DEBUG
export CONFIG_PATH=./godo.conf
else
export CONFIG_PATH=/etc/godo.conf
endif
export PERSIST_INTERVAL=600 # 10 minutes

# Folder configuration
PREFIX=/usr/local
export BINDIR=$(PREFIX)/bin
export ETCDIR=/etc
PAMDIR=$(ETCDIR)/pam.d
export SHAREDIR=$(PREFIX)/share
MANDIR=$(SHAREDIR)/man/man1
export EXAMPLEDIR=$(SHAREDIR)/godo

# Makefile configuration
DESTDIR=
YACCFLAGS=-d
LEXFLAGS=
CFLAGS=-pipe -Wall -Wno-unused-function -Werror -D_GNU_SOURCE
LDFLAGS=$(CFLAGS)
STRIP=strip
STRIPFLAGS=--strip-unneeded
INSTALL=install
INSTALLFLAGS=
CUNITS=y.tab lex.yy parser-utils ptrpool main match login cmdline runcmd cfgfile env temp
PODUNITS=godo.1
export BUILD_UUID=$(shell uuidgen -t)
export BINNAME=godo
BINFILE=./$(BINNAME)
CONFFILE=./$(BINNAME).conf
PAMFILE=./$(BINNAME)-pam.conf
COBJECTS=$(addsuffix .o,$(CUNITS))
MANFILES=$(addprefix man/,$(PODUNITS))
LIBS=-lpam

ifdef DEBUG
LEXFLAGS +=
YACCFLAGS += -t -v
CFLAGS += -ggdb
else
CFLAGS += -DNDEBUG -O2
endif

.PHONY: clean test install doc man

all: $(BINFILE) man

$(BINFILE): $(COBJECTS)
	$(CC) $(LDFLAGS) $(LIBS) -o $@ $^
ifndef DEBUG
	$(STRIP) $(STRIPFLAGS) $@
endif

$(COBJECTS): %.o: %.c config.h
	$(CC) -c $(CFLAGS) -o $@ $<

lex.yy.c: config.lex y.tab.c
	$(LEX) $(LEXFLAGS) config.lex

y.tab.c: config.y
	$(YACC) $(YACCFLAGS) config.y

config.h: config.h.in
		./subs.sh < $< > $@

clean:
	$(RM) y.tab.{c,h} lex.yy.c $(COBJECTS) y.output "$(BINFILE)" config.h $(MANFILES)

test: $(COBJECTS)
	$(BINFILE) -C test.conf

doc: man README.md

man: $(MANFILES)

$(MANFILES): %: %.pod
	./subs.sh < $< | pod2man -u --center="Execute command as another user or group" --date="October 2016" --name="$(BINNAME)" --release="$(APP_VERSION_MAJOR).$(APP_VERSION_MINOR).$(APP_VERSION_PATCH)-$(APP_VERSION_PHASE)" --section=1 > $@

README.md: man/godo.1.pod
	./subs.sh < $< | pod2markdown -m > $@

install: DESTFILE=$(DESTDIR)$(BINDIR)/$(BINNAME)
install: DESTCONF=$(DESTDIR)$(ETCDIR)/$(BINNAME).conf
install: DESTPAM=$(DESTDIR)$(PAMDIR)/$(BINNAME)
install: DESTEXAMPLE=$(DESTDIR)$(EXAMPLEDIR)/example.conf
install: $(BINFILE) $(CONFFILE) man install-dirs
ifdef DEBUG
	$(warning WARNING: Installing a debug binary)
endif
	$(INSTALL) $(INSTALLFLAGS) -m6755 "$(BINFILE)" "$(DESTFILE)"
	$(INSTALL) $(INSTALLFLAGS) $(MANFILES) $(MANDIR)/
	$(INSTALL) $(INSTALLFLAGS) $(CONFFILE) $(DESTEXAMPLE)
	-[[ ! -e "$(DESTCONF)" ]] && $(INSTALL) $(INSTALLFLAGS) -m640 "$(CONFFILE)" "$(DESTCONF)"
	-[[ ! -e "$(DESTPAM)" ]] && $(INSTALL) $(INSTALLFLAGS) -m644 "$(PAMFILE)" "$(DESTPAM)"

install-dirs:
	$(INSTALL) -d -m755 $(DESTDIR){$(BINDIR),$(PAMDIR),$(MANDIR),$(EXAMPLEDIR)}
