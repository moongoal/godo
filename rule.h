/*
 * Copyright (c) 2016, Alfredo Mungo
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef _RULE_H
#define _RULE_H

#include <stdbool.h>

#define RULEOPT_KEEPENV 0x00000001 /* Keep environment */
#define RULEOPT_SETENV  0x00000002 /* Set new environment */
#define RULEOPT_NOPASS  0x00000004 /* Don't require password */
#define RULEOPT_PERSIST 0x00000008 /* Don't require password for some time */

/*
  Identity.
*/
struct RULE_IDENTITY {
  char *username /* Username */, *group /* Group */;
};

/*
  Rule environment variable.
*/
struct RULE_VARIABLE {
  char *name /* Name */, *value /* Content */;
};

/*
  Configuration file rule.
*/
struct RULE {
  bool permit; /* Permit or deny? */
  unsigned long int options; /* Option flags */
  struct RULE_VARIABLE **env; /* Environment variables (NULL-terminated array) */
  struct RULE_IDENTITY subject /* Identity for which this rule applies */, target /* Identity as which the command will be executed */;
  char *cmd; /* Command which can/cannot be run */
  char **args; /* Allowed command line arguments for cmd */
  bool has_args; /* Is the "args" keyword present in the rule? */
};

#endif /* _RULE_H */
