/*
 * Copyright (c) 2016, Alfredo Mungo
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef _PTRPOOL_H
#define _PTRPOOL_H

#include <stdint.h>

/*
  Pointer pool. This data structure represents a pool of pointers which
  can only (and automatically) grow.
*/
struct PTRPOOL {
  void **pool; /* Array of pointers */
  size_t sz; /* Length of pool */
  size_t cap /* Current capacity */, cap_initial /* Initial capacity */, cap_incr /* Capacity increase step */;
};

/*
  Pool clean function.
  This function is used to clean the pool elements and is called against
  each pointer in the pool upon pp_cleanall() execution.

  ARGUMENTS
    ptr: Pointer to clean
*/
typedef void (*PP_CLEANFUNC)(void *ptr);

/*
  Initialise pool for usage.

  ARGUMENTS
    p: Pointer to pool structure to initialize
    initial_cap: Initial capacity
    cap_incr: Pool capacity increase step
*/
void pp_init(struct PTRPOOL *p, size_t initial_cap, size_t cap_incr);

/*
  Clean the pool.
  After calling this function, the pool structure shall not be used again
  without initialising it again first.

  ARGUMENTS
    p: The pool to clean
*/
void pp_clean(struct PTRPOOL *p);

/*
  Same as pp_clean() but also applie the clean function `f` to every element
  inside the pool.

  ARGUMENTS
    p: The pool to clean
    f: The clean function to use
*/
void pp_cleanall(struct PTRPOOL* p, PP_CLEANFUNC f);

/*
  Grow the pool if it has reached its capacity.

  ARGUMENTS
    p: The pool
*/
void pp_grow_if_needed(struct PTRPOOL *p);

/*
  Add an element to the pool.

  ARGUMENTS
    pool: The pool
    ptr: The pointer to add to the pool
*/
void pp_add(struct PTRPOOL *pool, void *ptr);

#endif /* _PTRPOOL_H */
