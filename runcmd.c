/*
 * Copyright (c) 2016, Alfredo Mungo
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
*/
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include "rule.h"
#include "match.h"
#include "debug.h"
#include "login.h"
#include "config.h"
#include "array.h"
#include "env.h"
#include "runcmd.h"

/*
  Run command as user and group with environment set.

  ARGUMENTS
    uid: User ID
    gid: Group ID
    cmd: Pointer to a null-terminated list of strings representing the command
         to be run and its arguments
    env: Pointer to a null-terminated list of environment variables with the
         same format as the `environ` variable (see `info environ`)

  NOTES
    If everything goes as expected, this routine does not return. If an error
    occurs, an error message is printed to stderr.
*/
static void run_command(uid_t uid, gid_t gid, char * const *cmd, char * const * env);

void run() {
  struct RULE **m = match();
  long i;

  NTA_LENGTH(m, i);

  dbgnotice2("Found %ld matching rules\n", i);

  if(i-1 >= 0) { /* At least one valid rule returned */
    struct RULE *mlast = m[i-1];

    if(mlast->permit) {
      uid_t tgt_uid;
      gid_t tgt_gid;
      struct passwd *user;
      struct group *group;

      if(mlast->target.username) { /* Username specified in rule */
        user = get_user(mlast->target.username);
      } else { /* Default or command line specified username */
        user = get_user(cl_username);
      }

      if(user) { /* User exists */
        tgt_uid = user->pw_uid;
      } else {
        dbgerr("Specified user does not exist.\n");
        exit_value = EXIT_FAILURE;
        goto end;
      }

      if(mlast->target.group) { /* Group specified in rule */
        group = get_group(mlast->target.group);
      } else { /* Default or command line specified group */
        group = get_group(cl_group);
      }

      if(group) { /* Group exists */
        tgt_gid = group->gr_gid;
      } else {
        dbgerr("Specified group does not exist.\n");
        exit_value = EXIT_FAILURE;
        goto end;
      }

      {
        char **env;
        char **cmd;
        size_t envn = 0;
        char *defenv_default[] = { /* Default environment to keep if KEEPENV is not specified */
          "USER",
          "LOGNAME",
          "HOME",
          "PATH",
          "TERM",
          "MAIL",
          "DISPLAY",
          NULL
        };
        char **defenv = (mlast->options & RULEOPT_KEEPENV)? getenv_all_names(environ): defenv_default;
        size_t defenvn;

        NTA_LENGTH(defenv, defenvn);

        if(mlast->env)
          while(mlast->env[envn++]);

        envn += defenvn;
        env = calloc(envn + 1, sizeof(char *));

        /* Set default environment */
        for(size_t i = 0; defenv[i]; i++)
          setenv2(env, defenv[i], getenv(defenv[i]));

        if(mlast->env) /* Set custom environment */
          for(size_t i = 0; mlast->env[i]; i++)
            setenv2(env, mlast->env[i]->name, mlast->env[i]->value);

        /* Set-up command */
        cmd = calloc(cl_command_len + 1, sizeof(char *));

        for(size_t i = 0; i < cl_command_len; i++) {
          cmd[i] = malloc(sizeof(char) * (1 + strlen(cl_command[i])));
          strcpy(cmd[i], cl_command[i]);
        }

        #define RUNCMD run_command(tgt_uid, tgt_gid, cmd, env)
        if(mlast->options & RULEOPT_NOPASS)
          RUNCMD;
        else if(mlast->options & RULEOPT_PERSIST && login_persists()) {
          login_set_persistent();

          RUNCMD;
        }
        else if(authenticate()) {
          if(mlast->options & RULEOPT_PERSIST) {
            login_set_persistent();
          }

          RUNCMD;
        }

        /* If we are here, an error occurred or it's a dry run. Let's clean up */
        A_FREE(cmd, cl_command_len);
        NTA_FREE(env);

        if(defenv != defenv_default) /* Dynamically allocated environment variable names */
          NTA_FREE(defenv);

        exit_value = dry_run? EXIT_SUCCESS: EXIT_FAILURE;
        goto end;
      }
    } else {
      dbgerr("You don't have the permission to do this.\n");
      exit_value = EXIT_FAILURE;
      goto end;
    }
  } else {
    dbgerr("You don't have the permission to do this.\n");
    exit_value = EXIT_FAILURE;
  }

  end: free(m);
}

static void run_command(uid_t uid, gid_t gid, char * const *cmd, char * const * env) {
  if(setuid(uid) == 0)
    if(setgid(gid) == 0) {
      if(!dry_run)
        execvpe(cmd[0], cmd, env);
      else
        return; /* Avoid debug-printing errno since there is no error here */
    }

  dbgerrno;
}
