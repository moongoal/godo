/*
 * Copyright (c) 2016, Alfredo Mungo
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef _CONFIG_H
#define _CONFIG_H

/* Default user and password for running processes */
#define DEFAULT_USER "root"
#define DEFAULT_GROUP "root"

/* Default temporary files directory */
#define DEFAULT_TEMPDIR "/tmp"

/* Configuration file path buffer length */
#define CFGPATH_LEN 2048

/* Shell path buffer length */
#define SHELLBUFF_LEN 256

/* User identity string buffer length */
#define IDENTITY_LEN 256

/* Prompt buffer length */
#define PROMPT_BUFF_LEN 1024

/* Configuration file path */
#define CONFIG_PATH "@@CONFIG_PATH@@"

/* Login persistance interval */
#define PERSIST_INTERVAL @@PERSIST_INTERVAL@@

#define _APP_H_VERSION_STR2(major, minor, patch) (#major "." #minor "." #patch)
#define _APP_H_VERSION_STR(major, minor, patch) _APP_H_VERSION_STR2(major, minor, patch)

#define APP_NAME "@@APP_NAME@@"
#define BUILD_UUID "@@BUILD_UUID@@"

#define APP_VERSION_MAJOR @@APP_VERSION_MAJOR@@
#define APP_VERSION_MINOR @@APP_VERSION_MINOR@@
#define APP_VERSION_PATCH @@APP_VERSION_PATCH@@
#define APP_VERSION_PHASE "@@APP_VERSION_PHASE@@"

#define APP_VERSION _APP_H_VERSION_STR(APP_VERSION_MAJOR, APP_VERSION_MINOR, APP_VERSION_PATCH)

#include "globals.h"

#endif /* _CONFIG_H */
